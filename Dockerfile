FROM python:3.7

ENV PYTHONUNBUFFERED=1
ENV ROOT=/usr/src/app

RUN apt-get update && apt-get -y install cmake

WORKDIR ${ROOT}

# Add requirements now (allows caching of pip dependencies for faster builds)
ADD requirements.txt ${ROOT}/requirements.txt

# Install requirements
RUN pip install -r requirements.txt

# Add everything else the app may need (including templates)
# into the container.
ADD . ${ROOT}

CMD ["uvicorn", "cipher.asgi:app", "--host", "0.0.0.0", "--workers", "4"]
