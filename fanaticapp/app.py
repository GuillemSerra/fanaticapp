from starlette.applications import Starlette

from fanaticapp import routes, settings, db


app = Starlette(debug=settings.DEBUG)


@app.on_event('startup')
async def build_app():
    routes.inject(app)
    await db.database.connect()


@app.on_event('shutdown')
async def close_app():
    await db.database.disconnect()
