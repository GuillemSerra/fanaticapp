import logging

from starlette.config import Config

config = Config(".env")

logging.basicConfig(level=logging.DEBUG, format='%(name)s: %(asctime)s (%(funcName)s) %(message)s')

DEBUG = config("DEBUG", default=False, cast=bool)

DB_URL = config("DB_URL", cast=str)
