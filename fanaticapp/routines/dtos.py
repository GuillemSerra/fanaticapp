from dataclasses import dataclass
from typing import List, Optional


@dataclass
class ExerciseDTO:

    name: str
    imageURL: Optional[str] = None
    videoURL: Optional[str] = None


@dataclass
class BlockDTO:

    type: str
    time: Optional[int] = None
    restTime: Optional[int] = None
    series: Optional[int] = None
    repetitions: Optional[int] = None
    goal: Optional[str] = None
    exercises: Optional[List[dict]] = None


@dataclass
class CreateRoutineDTO:

    name: str
    blocks: List[BlockDTO]
