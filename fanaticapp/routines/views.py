from marshmallow import ValidationError
from starlette.endpoints import HTTPEndpoint
from starlette.responses import JSONResponse
from starlette.requests import Request

from .domain import factories
from .domain.factories import build_get_routine_use_case, build_delete_routine_use_case
from .validators import CreateRoutineValidator


class Routines(HTTPEndpoint):

    async def get(self, request: Request) -> JSONResponse:
        use_case = build_get_routine_use_case()
        routine_id = request.path_params['routine_id']

        routine = await use_case.execute(routine_id)

        return JSONResponse(routine, status_code=200)

    async def post(self, request: Request) -> JSONResponse:
        routine_validator = CreateRoutineValidator()

        try:
            routine_dto = routine_validator.load(await request.json())
        except ValidationError as errors:
            return JSONResponse({'errors': errors.messages}, status_code=400)

        use_case = factories.build_create_routine_use_case()
        result = await use_case.execute(routine_dto)

        return JSONResponse(
            result,
            status_code=201
        )

    async def delete(self, request: Request) -> JSONResponse:
        use_case = build_delete_routine_use_case()
        routine_id = request.path_params['routine_id']

        await use_case.execute(routine_id)
        return JSONResponse(status_code=200)
