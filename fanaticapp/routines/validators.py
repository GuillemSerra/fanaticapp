from marshmallow import fields, Schema, post_load

from .dtos import BlockDTO, ExerciseDTO, CreateRoutineDTO


class CreateRoutineValidator(Schema):

    class BlockValidator(Schema):

        class ExerciseValidator(Schema):
            name = fields.Str(required=True)
            imageURL = fields.Str()
            videoURL = fields.Str()

            @post_load
            def build_exercise_dto(self, data, *args, **kwargs) -> dict:
                return data

        type = fields.Str(required=True)
        time = fields.Int()
        restTime = fields.Int()
        series = fields.Int()
        repetitions = fields.Int()
        goal = fields.Str()
        exercises = fields.Nested(ExerciseValidator, many=True)

        @post_load
        def build_block_dto(self, data, *args, **kwargs) -> dict:
            return data

    name = fields.Str(required=True)
    blocks = fields.Nested(BlockValidator, many=True, required=True)

    @post_load
    def build_routine_dto(self, data, *args, **kwargs) -> CreateRoutineDTO:
        return CreateRoutineDTO(**data)
