from fanaticapp.routines.dtos import CreateRoutineDTO


class GetRoutineUseCase:

    def __init__(self, routines_repo):
        self._routines_repo = routines_repo

    async def execute(self, id: int) -> dict:
        return await self._routines_repo.get(id)


class CreateRoutineUseCase:

    def __init__(self, routines_repo):
        self._routines_repo = routines_repo

    async def execute(self, data: CreateRoutineDTO) -> dict:
        return await self._routines_repo.create(data)


class DeleteRoutineUseCase:

    def __init__(self, routines_repo):
        self._routines_repo = routines_repo

    async def execute(self, id: int) -> dict:
        return await self._routines_repo.delete(id)
