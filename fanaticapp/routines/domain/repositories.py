from fanaticapp.routines.dtos import CreateRoutineDTO
from fanaticapp.routines.models import Routine


class RoutinesRepository:

    @staticmethod
    async def get(id: int) -> dict:
        return (await Routine.objects.get(id=id)).__dict__

    @staticmethod
    async def create(routine: CreateRoutineDTO) -> dict:
        new_routine = await Routine.objects.create(name=routine.name, blocks=routine.blocks)
        return new_routine.__dict__

    @staticmethod
    async def delete(routine_id: int):
        routine = await Routine.objects.get(id=routine_id)
        await routine.delete()
