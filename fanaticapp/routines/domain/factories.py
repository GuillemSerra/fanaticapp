from .use_cases import CreateRoutineUseCase, GetRoutineUseCase, DeleteRoutineUseCase
from .repositories import RoutinesRepository


def build_get_routine_use_case() -> GetRoutineUseCase:
    return GetRoutineUseCase(
        routines_repo=build_routines_repo()
    )


def build_create_routine_use_case() -> CreateRoutineUseCase:
    return CreateRoutineUseCase(
        routines_repo=build_routines_repo()
    )


def build_delete_routine_use_case() -> DeleteRoutineUseCase:
    return DeleteRoutineUseCase(
        routines_repo=build_routines_repo()
    )


def build_routines_repo() -> RoutinesRepository:
    return RoutinesRepository()
