import orm

from fanaticapp.db import database, metadata


class Routine(orm.Model):

    __tablename__ = "routines"
    __database__ = database
    __metadata__ = metadata

    id = orm.Integer(primary_key=True)
    name = orm.String(max_length=255)
    blocks = orm.JSON()
