from starlette.applications import Starlette

from fanaticapp.routines.views import Routines


def inject(app: Starlette) -> Starlette:
    routes = [
        ['/v1/routines', Routines, ["POST"]],
        ['/v1/routines/{routine_id:int}', Routines, ["GET", "DELETE"]]
    ]

    for route in routes:
        app.add_route(*route)

    return app
