import aiodine
import databases
import sqlalchemy

import fanaticapp.settings as settings


database = databases.Database(settings.DB_URL, min_size=10, max_size=50)
metadata = sqlalchemy.MetaData()


@aiodine.provider
async def db_transaction():
    async with database.transaction():
        yield
